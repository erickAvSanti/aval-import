<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('image_name',200);
            $table->string('image_square_name',200);
            $table->text('description');
            $table->boolean('price_last_es_show');
            $table->boolean('price_es_show');
            $table->boolean('price_last_en_show');
            $table->boolean('price_en_show');
            $table->unsignedDecimal('price_last_es',9,2);
            $table->unsignedDecimal('price_es',9,2);
            $table->unsignedDecimal('price_last_en',9,2);
            $table->unsignedDecimal('price_en',9,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
