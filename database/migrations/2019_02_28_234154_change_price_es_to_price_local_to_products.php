<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePriceEsToPriceLocalToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('price_es');
            $table->dropColumn('price_last_es');
            $table->dropColumn('price_es_show');
            $table->dropColumn('price_last_es_show');

            $table->boolean('price_local_show'); 
            $table->boolean('price_last_local_show');
            $table->unsignedDecimal('price_last_local',9,2);
            $table->unsignedDecimal('price_local',9,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedDecimal('price_es',9,2); 
            $table->unsignedDecimal('price_last_es',9,2);
            $table->boolean('price_es_show'); 
            $table->boolean('price_last_es_show');
            
            $table->dropColumn('price_local_show');
            $table->dropColumn('price_last_local_show');
            $table->dropColumn('price_local');
            $table->dropColumn('price_last_local');
        });
    }
}
