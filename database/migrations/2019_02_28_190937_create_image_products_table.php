<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_products', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->string('file_name',350);
            $table->unique(['product_id','file_name'],'product_id_file_name_unique');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('image_products', function (Blueprint $table) {
            $table->dropIndex('product_id_file_name_unique'); 
        });
        Schema::dropIfExists('image_products');
    }
}
