<?php
 
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();  
	    App\Product::query()->delete();
	    DB::statement('ALTER TABLE products AUTO_INCREMENT=1;');
	    for($i = 0; $i < 20; $i++) { 
	        App\Product::create([ 
	            'name' => $faker->name,
	            'image_name'=>'product.jpg',
	            'image_square_name'=>'product.jpg',
	            'description'=>$faker->realText(50),
	            'price_last_es_show'=>random_int(0,1),
	            'price_es_show'=>random_int(0,1),
	            'price_last_en_show'=>random_int(0,1),
	            'price_en_show'=>random_int(0,1),
	            'price_es'=>$faker->randomFloat(2,100,300),
	            'price_last_es'=>$faker->randomFloat(2,100,300),
	            'price_en'=>$faker->randomFloat(2,100,300),
	            'price_last_en'=>$faker->randomFloat(2,100,300),
	        ]);
	    } 
    }
}
