<?php

use Illuminate\Database\Seeder;
use \App\CategoryProduct;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  
        CategoryProduct::truncate();
	    for($i = 0; $i < 20; $i++) { 
	    	try {
		        CategoryProduct::create([ 
		            'category_id'=>random_int(1,10),
		            'product_id'=>random_int(1,20),
		        ]); 
            } catch (Illuminate\Database\QueryException $e) {  
            } catch (PDOException $e) { 
            }  
	    }  
    }
}
