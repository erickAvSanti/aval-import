<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        if(App\Category::count()<=0){
		    for($i = 0; $i < 10; $i++) {
		    	if($i>2){
			        App\Category::create([ 
			            'name' => $faker->name,
			            'parent_id'=>random_int(1,2),
			        ]);
		    	}else{
			        App\Category::create([ 
			            'name' => $faker->name, 
			        ]);
		    	}
		    }
        }
    }
}
