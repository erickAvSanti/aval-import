<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        App\Option::create([ 
            'k' => 'BUSINESS_NAME', 
            'v' => 'My Company', 
        ]);
        App\Option::create([ 
            'k' => 'BUSINESS_PHONE', 
            'v' => '(51)987654321', 
        ]);
        App\Option::create([ 
            'k' => 'BUSINESS_WHATSAPP_NUMBER', 
            'v' => '51987654321', 
        ]);
        App\Option::create([ 
            'k' => 'BUSINESS_FACEBOOK_PAGE', 
            'v' => 'http://www.facebook.com/abc', 
        ]);
        App\Option::create([ 
            'k' => 'BUSINESS_ADDRESS', 
            'v' => '5th Avenue', 
        ]);
        App\Option::create([ 
            'k' => 'BUSINESS_DATE_STARTED', 
            'v' => '05/05/2019', 
        ]);
    }
}
