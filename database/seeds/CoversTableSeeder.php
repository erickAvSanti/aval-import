<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Cover;

class CoversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        Cover::truncate();
	    for($i = 0; $i < 4; $i++) { 
	    	try {
		        Cover::create([ 
		            'title'=>$faker->realText(50),
		            'description'=>$faker->realText(300),
		            'image_name'=>'cover.jpg', 
		            'visible'=>random_int(0,1), 
		        ]); 
            } catch (Illuminate\Database\QueryException $e) {  
            } catch (PDOException $e) { 
            }  
	    }  
    }
}
