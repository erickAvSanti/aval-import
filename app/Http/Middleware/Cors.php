<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        //\Log::info($request->path());
        if($request->is('api/adm/*')){

            if ($request->getMethod() == "OPTIONS") {   
                $headers = [    
                    'Access-Control-Allow-Origin' => '*',    
                    'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',    
                    'Access-Control-Allow-Headers' => 'X-API-KEY, X-ACCESS-TOKEN, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Cache-Control, Authorization' 
                ];
                return \Response::make('OK', 200, $headers);    
            }   


            return $next($request)
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', "POST, GET, OPTIONS, PUT, DELETE")
                ->header("Access-Control-Allow-Headers", "X-API-KEY, X-ACCESS-TOKEN, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Cache-Control, Authorization");
        }
        return $next($request);
    }
}
