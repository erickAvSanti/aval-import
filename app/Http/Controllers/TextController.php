<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class TextController extends Controller
{
    //
    public function saveTermsOfServices(Request $request){
    	$html = $request->html;
    	Storage::disk('public_storage')->put('terms-of-service.txt', $html); 
		return response()->json(["msg"=>"ok"],200);
    }
    public function getTermsOfServices(Request $request){ 
    	$content = Storage::disk('public_storage')->get('terms-of-service.txt'); 
		return response()->json(["content"=>$content],200);
    }
}
