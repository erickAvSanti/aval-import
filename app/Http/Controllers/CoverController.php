<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Cover;
use Illuminate\Support\Facades\Storage;

class CoverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $data = Cover::all();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function images_upload(Request $request)
    {
        //
        $path = $request->file('cover')->store('covers',['disk'=>'public_storage']);
        //$path = $request->file('file')->move(public_path("/storage/covers"));
        //Storage::disk('uploads')->put('filename', $file_content);
        return $path; 
    }
    public function images(Request $request)
    { 
        $images = Storage::disk('public_storage')->files('covers');
        return response()->json(['folder'=>'storage','images'=>$images],200);
    }
    public function remove_image($src)
    {  
        if(strpos($src,'/')!==false){
            $src = explode("/",$src);
            $src = $src[count($src)-1];
        }
        if(Storage::disk('public_storage')->delete('covers/'.$src)){
            return response()->json(['msg'=>'deleted'],200);
        }else{
            return response()->json(['msg'=>'cant-delete'],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $record = new Cover;
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"cant-create"],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private static function &fillData(&$request,&$model){

        $_image_name = $request->image_name ? $request->image_name : 'cover.jpg';
        if(strpos($_image_name,"/")!==false){
            $_image_name = explode("/",$_image_name);
            $_image_name = $_image_name[count($_image_name)-1];
        }

        $model->title = $request->title ? $request->title : '';
        $model->description = $request->description;
        $model->image_name = $_image_name;
        $model->visible = is_numeric($request->visible) ? $request->visible : 1;
        $model->url = $request->url;
        $model->url_blank = $request->url_blank ? $request->url_blank : 0; 
        return $model;
    }
    public function update(Request $request, $id)
    { 
        $record = Cover::find($id);
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Cover::find($id);
        if($data){
            if($data->delete()){
                return response()->json($data); 
            }else{
                return response()->json(["msg"=>"cant-delete"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
}
