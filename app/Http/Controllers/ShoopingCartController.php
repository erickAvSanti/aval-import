<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use \App\Cover;
use \App\Product;
use \App\Option;

use Cookie;

class ShoopingCartController extends Controller
{
    //
    public function show(Request $request){ 
        $categories = Category::list_parent_with_children();   
        $selected_products = Cookie::get(env('MIX_VUE_APP_SHOPPING_CART_COOKIE_NAME'));
        $selected_products = json_decode($selected_products,true); 
        $products = array();
        $product_ids = array();
        if(is_array($selected_products)){
        	foreach ($selected_products as $product_id => $product_quantity) {
        		$product_ids[] = $product_id;
        	}
        	if(count($product_ids)>0){
        		$products = Product::whereIn('id',$product_ids)->get();
        		foreach ($products as $key => &$product) {
        			$images = $product->with_images;
        			if($images){
        				$product->first_image = $images->first();
        			}
        		}
        	}
        }
        $opts = [
            'categories'=>$categories, 
            'selected_products'=>$selected_products, 
            'products'=>$products, 
          	'folder_image_products'=>WebsiteController::FOLDER_IMAGE_PRODUCTS,
            'meta_image_url'=>WebsiteController::LOGO_IMAGE, 
            'website_title'=>'Shooping Cart', 
            'options'=>Option::toHash(),
            'website_desc'=>WebsiteController::WEBSITE_DESCRIPTION, 
        ];
        return view('shooping_cart',$opts);
    }
}
