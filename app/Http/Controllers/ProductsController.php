<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Product;
use \App\ImageProduct;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $data = Product::all();
        foreach ($data as $key => &$value) {
            # code...
            $images = $value->with_images;
            foreach ($value->categories as $key_cat => $cat) {
                $cat->children;
            }
            $arr = array();
            foreach ($images as $img) {
                $arr[] = $img->file_name;
            }
            $value->images = $arr;
        }
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function images_upload(Request $request)
    {
        //
        $_file = $request->file('image_product');
        $_original_name = explode(".",$_file->getClientOriginalName());

        $_file_name = preg_replace("/[\s]+/","-",$_original_name[0])."-".time().".".$_original_name[1];
        \Log::info($_file_name);
        //$path = $request->file('image_product')->store('image_products',['disk'=>'public_storage']);
        $path = $_file->storeAs('image_products',$_file_name,['disk'=>'public_storage']);
        //$path = $request->file('file')->move(public_path("/storage/covers"));
        //Storage::disk('uploads')->put('filename', $file_content);
        return $path; 
    }
    public function images(Request $request)
    { 
        $folder2 = 'image_products';
        $images = Storage::disk('public_storage')->files($folder2);
        return response()->json(['folder'=>'storage','folder2'=>$folder2,'images'=>$images],200);
    }
    public function remove_image($src)
    {  
        if(strpos($src,'/')!==false){
            $src = explode("/",$src);
            $src = $src[count($src)-1];
        }
        if(Storage::disk('public_storage')->delete('image_products/'.$src)){
            return response()->json(['msg'=>'deleted'],200);
        }else{
            return response()->json(['msg'=>'cant-delete'],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $record = new Product;
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                if(is_array($request->images)){ 
                    foreach ($request->images as $key => $img) { 
                        $record->images()->insert([
                            'product_id'=>$record->id,
                            'file_name'=>$img,
                        ]); 
                    }
                }
                $record->images = $record->with_images;
                \Log::info($request->categories);
                if(is_array($request->categories)){
                    foreach ($request->categories as $key => $cat) {
                        $cat_selected = $cat['selected'];
                        $cat_children = $cat['children'];
                        if($cat_selected)$record->categories()->attach($cat['id']); 
                        if(is_array($cat_children)){
                            foreach ($cat_children as $child) {
                                if($child['selected'])$record->categories()->attach($child['id']); 
                            }
                        }
                    }
                }
                $record->categories;
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"cant-create"],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private static function &fillData(&$request,&$model){ 
        $model->name = $request->name ? $request->name : '';
        $model->description = $request->description ? $request->description : ''; 
        $model->visible = is_numeric($request->visible) ? $request->visible : 1;
        $model->price_local = is_numeric($request->price_local) ? $request->price_local : 0.0; 
        $model->price_last_local = is_numeric($request->price_last_local) ? $request->price_last_local : 0.0; 
        $model->price_foreign = is_numeric($request->price_foreign) ? $request->price_foreign : 0.0; 
        $model->price_last_foreign = is_numeric($request->price_last_foreign) ? $request->price_last_foreign : 0.0; 
        $model->price_local_show = is_numeric($request->price_local_show) ? $request->price_local_show : 1; 
        $model->price_foreign_show = is_numeric($request->price_foreign_show) ? $request->price_foreign_show : 1; 
        $model->price_last_local_show = is_numeric($request->price_last_local_show) ? $request->price_last_local_show : 1; 
        $model->price_last_foreign_show = is_numeric($request->price_last_foreign_show) ? $request->price_last_foreign_show : 1; 
        return $model;
    }
    public function update(Request $request, $id)
    { 
        $record = Product::find($id);
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                \Log::info($request->images);
                if(is_array($request->images)){
                    $record->images()->delete();
                    foreach ($request->images as $key => $img) { 
 
                        $record->images()->insert([
                            'product_id'=>$record->id,
                            'file_name'=>$img,
                        ]); 
                    }
                }
                $record->images = $record->with_images;
                \Log::info($request->categories);
                if(is_array($request->categories)){
                    $record->category_products()->delete();
                    foreach ($request->categories as $key => $cat) {
                        $cat_selected = $cat['selected'];
                        $cat_children = $cat['children'];
                        if($cat_selected)$record->categories()->attach($cat['id']); 
                        if(is_array($cat_children)){
                            foreach ($cat_children as $child) {
                                if($child['selected'])$record->categories()->attach($child['id']); 
                            }
                        }
                    }
                }
                $record->categories;
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
    public function highlight(Request $request, $id)
    { 
        $record = Product::find($id);
        if($record){
            $record->highlight = $request->highlight;
            if($record->save()){ 
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-set-highlight"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Product::find($id);
        if($data){
            if($data->delete()){
                return response()->json($data); 
            }else{
                return response()->json(["msg"=>"cant-delete"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
}
