<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Option;

class OptionController extends Controller
{
    //
    private static function &fillData(&$request,&$model){
    	$k = strtoupper($request->k);
    	$k = preg_replace("/[\s]+/","_",$k); 
        $model->k = $k;
        $model->v = $request->v; 
        return $model;
    }
    public function list()
    {
        $data = Option::all();
        return response()->json($data);
    }
    public function store(Request $request)
    { 
        $record = new Option; 
        $record = &self::fillData($request,$record);
        if($record->save()){
            return response()->json($record); 
        }else{
            return response()->json(["msg"=>"cant-save"],400);
        } 
    }
    public function update(Request $request, $id)
    { 
        $record = Option::find($id);
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
    public function destroy($id)
    {
        $data = Option::find($id);
        if($data){
            if($data->delete()){
                return response()->json($data); 
            }else{
                return response()->json(["msg"=>"cant-delete"],400);
            } 
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
}
