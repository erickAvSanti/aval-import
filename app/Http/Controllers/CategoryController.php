<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;

class CategoryController extends Controller
{
    //
    public function list(Request $request){
        $param = $request->param ? $request->param : '';
        if($param=='without_parent_and_with_children'){
            $data = &Category::without_parent_and_with_children();
        }else if($param=='with_children'){
            $data = &Category::with_children();
        }else{
            $data = Category::all();
        }
    	return response()->json($data);
    }
    private static function &fillData(&$request,&$model){ 
        $model->name = $request->name ? $request->name : '';   
        $model->parent_id = $request->parent_id ? $request->parent_id : null;   
        return $model;
    }
    public function store(Request $request)
    { 
        $record = new Category;
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"cant-create"],500);
        }
    }
    public function store_sub(Request $request)
    { 
        $record = new Category;
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"cant-create"],500);
        }
    }
    public function update(Request $request, $id)
    { 
        $record = Category::find($id);
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    } 
    public function update_sub(Request $request, $id)
    { 
        $record = Category::find($id);
        if($record){
            $record = &self::fillData($request,$record);
            if($record->save()){
                return response()->json($record); 
            }else{
                return response()->json(["msg"=>"cant-save"],400);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    } 
    public function destroy($id)
    {
        $data = Category::find($id);
        if($data){
            try{
                if($data->delete()){
                    return response()->json($data); 
                }else{
                    return response()->json(["msg"=>"cant-delete"],400);
                }
            }catch(\Illuminate\Database\QueryException $e){
                return response()->json(["msg"=>"cant-delete"],500);
            } 
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
    public function destroy_sub($id)
    {
        $data = Category::where('id',$id)->whereNotNull('parent_id')->first();
        if($data){
            try{
                if($data->delete()){
                    return response()->json($data); 
                }else{
                    return response()->json(["msg"=>"cant-delete"],400);
                }
            }catch(\Illuminate\Database\QueryException $e){
                return response()->json(["msg"=>"cant-delete"],500);
            }
            
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
    public function children($id)
    {
        $data = Category::get_children_with_id_name_parent_id($id);
        if($data){
            return response()->json($data); 
        }else{
            return response()->json(["msg"=>"not-found"],404);
        }
    }
    public function list_with_parents(){
    	$data = Category::all();  
    	$names = array();
    	foreach ($data as $key => &$value) {
    		$data =['reg'=>$value->toArray()];
    		$_parent = $value->parent;
    		if($_parent){
    			$data['parent'] = $_parent->name;
    		}else{
    			$data['parent'] = '';
    		}
     		$names[] = $data;
    	}
    	return response()->json($names);
    }
    public function list_parent_with_children(){
    	$names = Category::list_parent_with_children();
    	return response()->json($names);
    }
}
