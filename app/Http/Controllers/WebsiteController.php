<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use \App\Cover;
use \App\Product;
use \App\Option;
use Illuminate\Support\Facades\Storage;
class WebsiteController extends Controller
{
    // 
    const FOLDER_IMAGE_PRODUCTS = "storage/image_products/";
    const LOGO_IMAGE = "img/logo_square.jpg";
    const WEBSITE_TITLE = "Website Application";
    const WEBSITE_DESCRIPTION = "Website Description";
    public function index()
    {
        $categories = Category::list_parent_with_children();  
        $opts = [
            'categories'=>$categories,
            'options'=>Option::toHash(),
            'folder_image_products'=>self::FOLDER_IMAGE_PRODUCTS,
            'products'=>Product::where('highlight',1)->get(),
            'covers'=>Cover::all(),
            'meta_image_url'=>self::LOGO_IMAGE, 
            'website_title'=>self::WEBSITE_TITLE, 
            'website_desc'=>self::WEBSITE_DESCRIPTION, 
        ];
        return view('welcome',$opts);
    } 
    public function show_by_category($id,$name)
    { 
        $category = Category::find($id);
        $categories = Category::list_parent_with_children();
        if($category){ 
            $products = $category->products;
            $props = [
                'folder_image_products'=>self::FOLDER_IMAGE_PRODUCTS,
                'cat_id'=>$id,
                'cat_name'=>$category->name,
                'cat_parent'=>$category->parent,
                'categories'=>$categories,
                'products'=>$products,
                'options'=>Option::toHash(),
                'meta_image_url'=>self::LOGO_IMAGE, 
                'website_title'=>self::WEBSITE_TITLE, 
                'website_desc'=>self::WEBSITE_DESCRIPTION, 
            ];
            return view('website_by_category',$props);
        }else{
            abort(404); 
        }
    } 
    public function show_product($id,$name)
    { 
        $product = Product::find($id);
    	$categories = Category::list_parent_with_children();
        if($product){  
            $product_categories = $product->categories; 
            $images = $product->images;
            foreach ($product_categories as $key_p_c => &$prod_cat) {
                $prod_cat->parent;
                $prod_cat->children;
            }
            if(count($images)>0){
                $image = $images[0];
                $meta_image_url = self::FOLDER_IMAGE_PRODUCTS.$image->file_name;
            }else{
                $meta_image_url = self::LOGO_IMAGE;
            }
	        $props = [
                'folder_image_products'=>self::FOLDER_IMAGE_PRODUCTS, 
                'product'=>$product, 
                'images'=>$images, 
                'meta_image_url'=>$meta_image_url, 
                'website_title'=>$product->name, 
                'website_desc'=>$product->description, 
	        	'prod_categories'=>$product_categories,
                'categories'=>$categories, 
                'options'=>Option::toHash(),
	        ];
	        return view('website_by_product',$props);
        }else{
        	abort(404); 
        }
    }
    public function terms_of_services(){
        $contents = Storage::disk('public_storage')->get('terms-of-service.txt'); 
        $props = [  
            'options'=>Option::toHash(),
            'meta_image_url'=>self::LOGO_IMAGE, 
            'website_title'=>self::WEBSITE_TITLE, 
            'website_desc'=>self::WEBSITE_DESCRIPTION, 
            'text'=>$contents, 
        ];
        return view('terms_of_services',$props); 
    } 
}
