<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category; 
use \App\Product;
use \App\Option;

class SearchController extends Controller
{
    //
    public function search(Request $request){

    	$text = $request->text;
        if(!isset($text) || $text==""){
            $products = array();   
        }else{
            $products = Product::search($text);
        }
        \Log::info($products);
    	//find in sql
        $categories = Category::list_parent_with_children();  
        $opts = [
            'categories'=>$categories,  
            'products'=>$products,  
            'text'=>$text,  
          	'folder_image_products'=>WebsiteController::FOLDER_IMAGE_PRODUCTS,
            'meta_image_url'=>WebsiteController::LOGO_IMAGE, 
            'website_title'=>'Search', 
            'options'=>Option::toHash(),
            'website_desc'=>WebsiteController::WEBSITE_DESCRIPTION, 
        ];
        return view('search',$opts);
    }
}
