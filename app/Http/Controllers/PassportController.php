<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth; 

class PassportController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    const TOKEN_SEED  = 'Aval-import';
    public function list(Request $request)
    {
    	$request->merge(array_map('trim', $request->all()));
        return response()->json(User::all(), 200);
    }
    public function register(Request $request)
    {
    	$request->merge(array_map('trim', $request->all()));
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);
      	try {
 
	        $user = User::create([
	            'name' => $request->name,
	            'email' => $request->email,
	            'password' => bcrypt($request->password)
	        ]); 
        	return response()->json(['msg'=>'user_created'], 200);
        } catch (Illuminate\Database\QueryException $e) {
            return response()->json(['exc'=>'query','msg'=>$e->getMessage()], 500);

        } catch (PDOException $e) {
            return response()->json(['exc'=>'PDO','msg'=>$e->getMessage()], 500);
        }            
 
    }
 
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
    	$request->merge(array_map('trim', $request->all()));
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken(self::TOKEN_SEED)->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
 
    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }
    public function logout(){
    	$token = Auth::user()->token(); 
    	$revoked = $token->revoke();
    	\Log::info("revoked = ".$revoked);
        return response()->json(['wait'], 200);
    }
    public function update(Request $request,$id)
    {
    	$request->merge(array_map('trim', $request->all()));
    	$props = [
            'name' => 'required|min:3',
            'email' => 'required|email',
        ];
        if(
        	isset($request->password) && 
        	trim($request->password)!=''
        ){
        	$props['password'] = 'required|min:8';
        }
        $this->validate($request, $props);
    	$user = User::find($id);
    	if($user){
    		if(
    			isset($request->password) && 
    			trim($request->password)!=''
    		){
    			$user->password = bcrypt($request->password);
    		}
    		$user->name = $request->name;
    		$user->email = $request->email;

	      	try { 
		        $user->save();
	        	return response()->json(['msg'=>'user_updated'], 200);
	        } catch (Illuminate\Database\QueryException $e) {
	            return response()->json(['exc'=>'query','msg'=>$e->getMessage()], 500);

	        } catch (PDOException $e) {
	            return response()->json(['exc'=>'PDO','msg'=>$e->getMessage()], 500);
	        }  

    	}else{
            return response()->json(['error' => 'user_not_found'],404);
    	}
    }
    public function destroy(Request $request,$id)
    {
    	$request->merge(array_map('trim', $request->all()));
    	try{
    		$user =	User::find($id);
	    	$userTokens = $user->tokens;
	    	foreach($userTokens as $token) {
			    $token->revoke();   
			}
			$user->delete();
            return response()->json(['msg'=>'user_delteed'], 200);
    	} catch (Illuminate\Database\QueryException $e) {
            return response()->json(['exc'=>'query','msg'=>$e->getMessage()], 500);

        } catch (PDOException $e) {
            return response()->json(['exc'=>'PDO','msg'=>$e->getMessage()], 500);
        } catch (Exception $e) {
            return response()->json(['exc'=>'Exception','msg'=>$e->getMessage()], 500);
        }   
    }
}
