<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    public function children(){
    	return $this->hasMany(Category::class,'parent_id','id');
    }
    public static function &without_parent_and_with_children(){
        $data = Category::select('id','name')->whereNull('parent_id')->get();  
        $names = array();
        foreach ($data as $key => &$value) {
            $data = $value->toArray();
            $_children = $value->children_with_id_name;
            $data['children'] = $_children; 
            $data['children_size'] = $_children->count(); 
            $names[] = $data;
        }
        return $names;
    }
    public static function &with_children(){
        $data = Category::select('id','name')->whereNull('parent_id')->get();  
        $names = array();
        foreach ($data as $key => &$value) {
            $data = $value->toArray();
            $_children = $value->children_with_id_name_parent_id;
            $data['children'] = $_children; 
            $data['children_size'] = $_children->count(); 
            $names[] = $data;
        }
        return $names;
    } 
    public function children_with_id_name(){
        return $this->hasMany(Category::class,'parent_id','id')->select('id','name');
    }
    public function children_with_id_name_parent_id(){
        return $this->hasMany(Category::class,'parent_id','id')->select('id','name','parent_id');
    }
    public static function get_children_with_id_name($id){
        return Category::select('id','name')->where('parent_id',$id)->get(); 
    }
    public static function get_children_with_id_name_parent_id($id){
        return Category::select('id','name','parent_id')->where('parent_id',$id)->get(); 
    }
    public function parent(){
    	return $this->belongsTo(Category::class);
    }
    public function parent_with_id_name(){
    	return $this->belongsTo(Category::class)->select('id','name');
    }
    public static function list_parent_with_children(){
    	$data = Category::select('id','name','parent_id')->whereNull('parent_id')->get();  
    	$names = array();
    	foreach ($data as $key => &$value) {
            $data =[
                'reg'=>$value->toArray(), 
            ];
            $_children = $value->children_with_id_name;
            $data['children'] = $_children; 
            $data['children_size'] = $_children->count(); 
            $names[] = $data;
    	}
    	return $names;
    }
    public function products(){
        return $this->belongsToMany(Product::class,'category_product');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
