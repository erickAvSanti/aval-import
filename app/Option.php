<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //
    protected $fillable = [
    	'k',
    	'v', 
    ];

    /*
    protected $primaryKey='k';
    public $incrementing = false;
    */
    public static function toHash(){
    	$reg = Option::all();
    	$hash = [];
    	foreach ($reg as $key => $value) {
    		$hash[$value['k']] = $value['v'];
    	}
    	return $hash;
    }
}
