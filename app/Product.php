<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public static function search($str){
        return Product::where(function($query) use($str){
            $query->where('name','LIKE',"%$str%")->orWhere('description','LIKE',"%$str%");
        })->get();
    }
    public function categories(){
        return $this->belongsToMany(Category::class,'category_product');
    }
    public function category_products(){
        return $this->hasMany(CategoryProduct::class,'product_id','id');
    }
    public function with_images(){
        return $this->hasMany(ImageProduct::class,'product_id','id')
        	->select('file_name')
        	->orderBy('id','desc');
    }
    public function with_images_array_file_name(){
        $images = $this->hasMany(ImageProduct::class,'product_id','id')
        	->select('file_name')
        	->orderBy('id','desc');

        $arr = array();
        foreach ($images as $img) {
            $arr[] = $img->file_name;
        } 
    	return $arr;
    }
    public function images(){
        return $this->hasMany(ImageProduct::class,'product_id','id');
    }
}
