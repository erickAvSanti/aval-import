<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageProduct extends Model
{
    //
    protected $fillable = [
    	'product_id',
    	'file_name',
    ];
    public function product()
	{
	    return $this->belongsTo(Product::class,'product_id','id');
	}
}
