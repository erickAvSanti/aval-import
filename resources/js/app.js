
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import App from "./App.vue";
import QuantityProduct from "./components/QuantityProduct.vue";
import SocialShareProduct from "./components/SocialShareProduct.vue";
import ButtonActionsProduct from "./components/ButtonActionsProduct.vue";
import BasketProducts from "./components/BasketProducts.vue";
import ShoppingCartItem from "./components/ShoppingCartItem.vue"; 
import ShoppingCartForm from "./components/ShoppingCartForm.vue"; 
import SignInRegister from "./components/SignInRegister.vue"; 
import RecognitionSearch from "./components/RecognitionSearch.vue"; 

 
import VueInternationalization from 'vue-i18n';
import Locale from '../assets/js/vue-i18n-locales.generated';

Vue.use(VueInternationalization);

let lang = document.documentElement.lang.substr(0, 2);
if(typeof lang=='string')lang = lang.toLowerCase(); 
// or however you determine your current app locale
if(lang!='es' && lang!='en')lang='en';

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

if(document.getElementById('app') && false){

	Vue.component('example-component', require('./components/ExampleComponent.vue').default); 
	const app = new Vue({
	    el: '#app', 
	    components:{App},
	    template:'<app></app>'
	});
}
if(document.getElementById('quantity_product')){
	Vue.component('quantity-product',QuantityProduct); 
	const quantity_product = new Vue({
	    el: '#quantity_product',  
	});
}
if(document.getElementById('social_share_product')){
	Vue.component('social-share-product',SocialShareProduct); 
	const social_share_product = new Vue({
	    el: '#social_share_product',  
	}); 
}
if(document.getElementById('button_actions_product')){
	Vue.component('button-actions-product',ButtonActionsProduct); 
	const button_actions_product = new Vue({
	    el: '#button_actions_product',  
	});
}
Vue.component('basket-products',BasketProducts);
if(document.getElementById('basket_products')){
	const basket_products = new Vue({
	    el: '#basket_products',  
	});
} 
if(document.getElementById('top-header-responsive')){ 
	const to_header_responsive = new Vue({
	    el: '#top-header-responsive',  
	});
}
if(document.getElementById('selected_products')){
	Vue.component('shopping-cart-form',ShoppingCartForm);
	Vue.component('shopping-cart-item',ShoppingCartItem);
	const shopping_cart_item = new Vue({
	    el: '#selected_products',
	    i18n  
	});
}  

Vue.component('sign-in-register',SignInRegister);
if(
	document.getElementById('menu_responsive_header') 
){ 
	const sign_in_register1 = new Vue({
	    el: '#menu_responsive_header',
	    i18n  
	}); 
}  
if(
	document.getElementById('sign_in_register') 
){
	const sign_in_register2 = new Vue({
	    el: '#sign_in_register',
	    i18n  
	}); 
}  
Vue.component('recognition-search',RecognitionSearch);
if(
	document.getElementById('recognition-search') 
){
	const recognition_search = new Vue({
	    el: '#recognition-search',
	    i18n  
	}); 
}  
$(window).ready(function(){
	$('#header-owl-carousel').owlCarousel({  
		loop : true,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem: true,
		pagination: false,
		rewindSpeed: 500, 
		dots:true,  
		items: 1,
	});
	$('#owl-carousel-product').owlCarousel({  
		items:1,
	    lazyLoad:true,
	    loop:true,
	    margin:10
	});
	$("#open_menu_responsive").click(function(evt){
		$("body").toggleClass("menu-open");
	});
	$("#menu_overlay").click(function(evt){
		$("body").removeClass("menu-open");
	});
	onResize();
	$(window).resize(function(evt){
		onResize();
	});
	$(window).scroll(function(evt){
		var obj = $("#top-header");
		var obj2 = $("#menu_container");
		if(evt.currentTarget.pageYOffset>40){
			obj.addClass('header-collapse');
			obj2.addClass('header-collapse');
		}else{
			obj.removeClass('header-collapse');
			obj2.removeClass('header-collapse');
		}
	});
	$("#screen_menu_header_collapse").click(function(evt){
		$("body").toggleClass("header-collapse-visible");
	});
	$(document).keyup(function(e) { 
  		if (e.keyCode === 27){
			$("body").removeClass("header-collapse-visible");
  		}
	});
	$("#btn-search-responsive").click(function(evt){
		$("body").toggleClass("search-responsive");
	});
	$("#btn-search-responsive-close").click(function(evt){
		$("body").removeClass("search-responsive");
	});
	$("input[name=global_search]").keyup(function(e) {
		console.log(e); 
  		if (e.keyCode == 13){
			search_text($(this).val());
  		}
	});
});  
window.addEventListener("load",function(){
	console.log("files loaded");
	$("body").removeClass("website-mask");
});
function onResize(evt){
	var el = null;
	if(window.innerWidth<=767){
		if($("#menu_container").has("#menu_responsive_container").length>0){
			el = $("#menu_responsive_container").detach();
			$("body").append(el);
			var input_search = $("#input-search").detach();
			$("body").append(input_search);
			$("input[name=global_search]").keyup(function(e) {
				console.log(e); 
		  		if (e.keyCode == 13){
					search_text($(this).val());
		  		}
			});
		}
	}else{
		if($("#menu_container").has("#menu_responsive_container").length==0){
			el = $("#menu_responsive_container").detach();
			$("#menu_container").append(el);
			var input_search = $("#input-search").detach();
			$("#input-search-container").append(input_search);
			$("input[name=global_search]").keyup(function(e) { 
				console.log(e);
		  		if (e.keyCode == 13){
					search_text($(this).val());
		  		}
			});
		}
	}
	$("#close-menu-responsive").click(function(evt){
		$("body").removeClass("menu-open");
	});
};
function search_text(text){
	if(!text || (text=$.trim(text))==""){
		return;
	}
	window.location.assign("/search?text="+text);
}
window.setCookie = function(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
window.getCookie = function(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
	var c = ca[i];
	while (c.charAt(0) == ' ') {
	  c = c.substring(1);
	}
	if (c.indexOf(name) == 0) {
	  return c.substring(name.length, c.length);
	}
	}
	return "";
}
