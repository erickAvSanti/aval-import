<?php
return [
	'products'=>'Products',
	'what_are_you_looking_for'=>'¿What are you looking for?',
	'quantity'=>'Quantity',
	'product_detail'=>'Product Detail',
	'share'=>'SHARE',
	'unit_price'=>'Unit price',
	'all'=>'All',
	'terms_of_services'=>'Terms of services',
	'policy'=>'Policy',
	'store_information'=>'Store information',
	'my_account'=>'My Account',
	'orders'=>'Orders',
	'personal_information'=>'Personal information',
	'shopping_cart_empty'=>'Cart empty',
	'unregistered_products'=>'Unregistered products',
	'search_results'=>'Search results',
];