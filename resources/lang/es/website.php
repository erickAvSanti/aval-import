<?php
return [
	'products'=>'Productos',
	'what_are_you_looking_for'=>'¿Qué está buscando?',
	'quantity'=>'Cantidad',
	'product_detail'=>'Detalle de producto',
	'share'=>'COMPARTIR',
	'unit_price'=>'Precio unitario',
	'all'=>'Todos',
	'terms_of_services'=>'Términos y condiciones',
	'policy'=>'Políticas',
	'store_information'=>'Información de tienda',
	'my_account'=>'Mi cuenta',
	'orders'=>'Pedidos',
	'personal_information'=>'Información personal',
	'shopping_cart_empty'=>'Carrito vacío',
	'unregistered_products'=>'Productos no registrados',
	'search_results'=>'Resultados de búsqueda',
];