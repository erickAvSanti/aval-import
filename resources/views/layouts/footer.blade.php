			</main>
		    <div class="container">
		    	<footer id="cc_footer">
			        <ul>
			            <li>
			                <h3>@lang('website.my_account')</h3>
			                <ul>
			                    <li>@lang('website.personal_information')</li>
			                    <li>@lang('website.orders')</li>
			                </ul>
			            </li>
			            <li>
			                <h3>@lang('website.policy')</h3>
			                <ul>
			                    <li>
			                    	<a href="/terms-of-services"> 
				                    	<i class="fa fa-folder"></i>
				                    	<span>@lang('website.terms_of_services')</span>
			                    	</a>
			                    </li> 
			                </ul>
			            </li>
			            <li>
			                <h3>@lang('website.store_information')</h3>
			                <ul>
			                    <li>
			                    	<a href="tel://{{$options['BUSINESS_PHONE_NUMBER']}}"> 
				                    	<i class="fa fa-phone"></i>
				                    	<span>{{$options['BUSINESS_PHONE_NUMBER']}}</span>
			                    	</a>
			                    </li> 
			                    <li>
			                    	<a href="mailto://{{$options['BUSINESS_EMAIL']}}">
			                    		<i class="fa fa-envelope"></i>
			                    		<span>{{$options['BUSINESS_EMAIL']}}</span>
			                    	</a>
			                    </li> 
			                    <li>
			                    	<i class="fa fa-map-marker-alt"></i>
			                    	<span>{{$options['BUSINESS_ADDRESS']}}</span>
			                    </li> 
			                </ul>
			            </li>
			        </ul>
			        <ul>
			            <li> 
			            	<div class="btn btn-default">
			            		<a href="{{$options['BUSINESS_FACEBOOK_PAGE']}}">
			            			<i class="fab fa-facebook"></i>
			            		</a>
			            	</div>
			            	<div class="btn btn-default">
			            		<a href="{{$options['BUSINESS_TWITTER_PAGE']}}">
			            			<i class="fab fa-twitter"></i>
			            		</a>
			            	</div>
			            	<div class="btn btn-default">
			            		<a href="{{$options['BUSINESS_INSTAGRAM_PAGE']}}">
			            			<i class="fab fa-instagram"></i>
			            		</a>
			            	</div>
			            	<div class="btn btn-default">
			            		<a href="{{$options['BUSINESS_YOUTUBE_PAGE']}}">
			            			<i class="fab fa-youtube"></i>
			            		</a>
			            	</div>
			            </li>
			            <li> 
			            </li>
			            <li> 
			            </li>
			        </ul>
			    </footer>
		    </div>
			<div class="footer_corp">
				<p>{{$options['BUSINESS_NAME']}} - Todos los Derechos Reservados <span id="id_year">2019</span></p>
			</div>
	        <script type="text/javascript" src="{{mix('js/app.js')}}"></script> 
			<!-- Load Facebook SDK for JavaScript -->
			<div id="fb-root"></div>
        </div>
		<script>

			window.fbAsyncInit = function() {
    			FB.init({
	      			appId      : '{{env('MIX_VUE_APP_FB_APP_ID')}}',
	      			xfbml      : true,
	      			version    : '{{env('MIX_VUE_APP_FB_VERSION')}}'
	    		});
  			};


			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "https://connect.facebook.net/en_US/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>

		<script>
			window.twttr = (function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0],
				t = window.twttr || {};
				if (d.getElementById(id)) return t;
				js = d.createElement(s);
				js.id = id;
				js.src = "https://platform.twitter.com/widgets.js";
				fjs.parentNode.insertBefore(js, fjs);

				t._e = [];
				t.ready = function(f) {
					t._e.push(f);
				};

				return t;
			}(document, "script", "twitter-wjs"));
		</script>
    </body>
</html>