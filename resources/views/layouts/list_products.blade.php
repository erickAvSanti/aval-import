<ul class="top-products">
    @foreach ($products as $product) 
        @php 
            $image = $product->images()->first();
        @endphp 
        @if(isset($image))
            <li>
                <a href="/product/{{$product->id."-".str_slug($product->name)}}">
                    <div class="card card-product" style="width: 100%">
                        <img class="card-img-top" src="{{URL::to($folder_image_products.$image->file_name)}}" alt="{{$product->name}}">
                        <div class="card-body">
                            <label class="card-title">{{$product->name}}</label>
                            <div class="card-text">
                                <div class="price">
                                    @if($product->price_local_show==1)
                                        <label>
                                            {{env('CURRENCY_LOCAL')}} {{$product->price_local}}
                                        </label>
                                    @else
                                        <label>
                                            {{env('CURRENCY_LOCAL')}} --
                                        </label>
                                    @endif
                                    @if($product->price_last_local_show==1)
                                        <label>
                                            {{env('CURRENCY_LOCAL')}} {{$product->price_last_local}}
                                        </label>
                                    @endif
                                </div>
                                <div class="price">
                                    @if($product->price_foreign_show==1)
                                        <label>
                                            {{env('CURRENCY_FOREIGN')}} {{$product->price_foreign}}
                                        </label>
                                    @else
                                        <label>
                                            {{env('CURRENCY_FOREIGN')}} --
                                        </label>
                                    @endif
                                    @if($product->price_last_foreign_show==1)
                                        <label>
                                            {{env('CURRENCY_FOREIGN')}} {{$product->price_last_foreign}}
                                        </label>
                                    @endif
                                </div>
                            </div>   
                        </div>
                    </div>
                </a>
            </li>
        @else
            <li>
                <a href="/product/{{$product->id."-".str_slug($product->name)}}">
                    <div class="card" style="width: 100%">
                        <img class="card-img-top" src="{{URL::to('img/logo_square.jpg')}}" alt="Card image cap">
                        <div class="card-body">
                            <label class="card-title">{{$product->name}}</label>
                            <div class="card-text">
                                <div>{{env('CURRENCY_LOCAL')}} {{$product->price_local}}</div>
                                <div>{{env('CURRENCY_FOREIGN')}} {{$product->price_foreign}}</div>
                            </div>   
                        </div>
                    </div>
               </a>
            </li>
        @endif
    @endforeach 
</ul>