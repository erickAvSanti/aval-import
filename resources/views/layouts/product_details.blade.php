<h1>{{$product->name}}</h1>
<label class="price_local">
    <strong>{{env('CURRENCY_LOCAL').' '.$product->price_local}}</strong>
    @if($product->price_last_local_show==1)
        <span>{{env('CURRENCY_LOCAL').' '.$product->price_last_local}}</span>
    @endif
</label><br />
<label class="price_foreign">
    <strong>{{env('CURRENCY_FOREIGN').' '.$product->price_foreign}}</strong>
    @if($product->price_last_foreign_show==1)
        <span>{{env('CURRENCY_FOREIGN').' '.$product->price_last_foreign}}</span>
    @endif
</label><br />
<span>@lang('website.quantity'):</span>
<div id="quantity_product"> 
    <quantity-product></quantity-product> 
</div>
<hr />
<div> 
    <div id="button_actions_product"> 
        <button-actions-product :product_id="{{$product->id}}"></button-actions-product> 
    </div>
</div>
<hr />
<div>
    <strong>@lang('website.product_detail')</strong>
    <div>
        {!!$product->description!!}
    </div>
</div>
<hr />
<div>
    <strong>@lang('website.share')</strong>
    <div id="social_share_product">  
        <social-share-product image_src="{{url('').'/'.$meta_image_url}}" desc="{{$product->name}}" href="{{url()->full()}}"></social-share-product>  
    </div>
</div>