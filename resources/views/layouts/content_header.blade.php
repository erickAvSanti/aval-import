 
    @isset($categories)
    <div id="menu_container"> 
        <div id="menu_responsive_container">
            <div id="menu_responsive_header">
                <div class="container">
                    <div class="row vertical-align">
                        <div class="col-3 col-sm-4 cc-left" > 
                            <div class="business-tag-floating">
                                <a class="business-tag" href="{{$options['BUSINESS_WHATSAPP_URL']}}">
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                                <a class="business-tag" href="tel://{{$options['BUSINESS_PHONE_NUMBER']}}">
                                    <i class="fa fa-phone"> 
                                    </i>
                                    <span>{{$options['BUSINESS_PHONE_NUMBER']}}</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4">
                            <div class="cc-center">
                                <a href="/">
                                    <img class="img-logo" src="{{ URL::to(env('BUSINESS_IMG_LOGO')) }}" />
                                </a>
                            </div>
                        </div>
                        <div class="col-3 col-sm-4 cc-right">
                            
                            
                            <div class="btn btn-default" id="close-menu-responsive">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>
                </div> 
                <sign-in-register csrf_token='{{csrf_token()}}'></sign-in-register>
            </div>
            <nav>
                <ul class="list-categories">
                    @foreach ($categories as $category) 
                        @if($category['children_size']>0)
                            <li>
                                <label>{{$category['reg']['name']}}</label>
                                <ul class="list-sub-categories ul-hidden">
                                    <li> 
                                        <a href="/category-{{$category['reg']['id'].'-'.str_slug($category['reg']['name'])}}">
                                            <label>@lang('website.all')</label>
                                        </a> 
                                    </li>
                                    @foreach ($category['children'] as $category_children)
                                        <li>
                                            <a href="/category-{{$category_children['id'].'-'.str_slug($category_children['name'])}}">
                                                <label>
                                                    {{$category_children['name']}}
                                                </label>
                                            </a>
                                        </li> 
                                    @endforeach
                                </ul>
                            </li>
                        @else 
                            <li> 
                                <a href="./category-{{$category['reg']['id'].'-'.str_slug($category['reg']['name'])}}">
                                    <label>{{$category['reg']['name']}}</label>
                                </a> 
                            </li>
                        @endif
                    @endforeach 
                </ul>
            </nav>
        </div>
    </div>
    @endisset 
