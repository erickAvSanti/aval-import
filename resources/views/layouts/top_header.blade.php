<!doctype html>
@php
    /*
    if(!isset($website_title))$website_title = '';
    if(!isset($website_desc))$website_desc = '';
    */
@endphp
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, minimum-scale=1,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta http-equiv="X-UA-Compatible" content="IE=8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{$website_title}}</title>
        <!-- You can use Open Graph tags to customize link previews.
        Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
        <meta property="og:url"           content="{{url()->full()}}" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="{{$website_title}}" />
        <meta property="og:description"   content="{{$website_desc}}" />
        <meta property="og:image"         content="{{url('').'/'.$meta_image_url}}" /> 
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#008000">
        <meta name="msapplication-TileColor" content="#008000">
        <meta name="theme-color" content="#008000">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> 
        <!-- Styles -->
        <link href="{{mix('css/app.css')}}" rel="stylesheet"> 
        <style type="text/css">
            #website_mask{
                display: none;
                background:#fff;
                position: fixed;
                top:0;
                left: 0;
                width: 100%;
                height: 100vh;
                z-index: 50000;
            }
            .website-mask #website_mask{
                display: block; 
            }
            .website-mask{
                overflow: hidden;
            }
            #website_mask .spinner { 
                box-sizing: border-box;
                width: 60px;
                height: 60px;
                border-radius: 50%;
                border: 5px solid #f3f3f3;
                border-top: 5px solid #3498db;
                animation: spin 2s linear infinite;
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                right: 0;
                margin: auto;
            } 
            @keyframes spin {
                0% {
                    transform: rotate(0deg);
                }
                100% {
                    transform: rotate(360deg);
                }
            }
        </style>
    </head>
    <body class="website-mask">
        <div id="loading_shopping_cart">
            <div> 
                <div class="spinner-border text-success" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <span>Procesando...</span> 
            </div>
        </div>
        <div id="website_mask">
            <div class="spinner"></div>
        </div>
        <header id="top-header-responsive">
            <div class="container">
                <div class="row vertical-align">
                    <div class="col-2 col-sm-4">
                        <div id="open_menu_responsive" class="fas fa-align-justify"></div>
                    </div>
                    <div class="col-8 col-sm-4">
                        <div class="cc-center">
                            <a href="/">
                                <img class="img-logo" src="{{ URL::to(env('BUSINESS_IMG_LOGO')) }}" />
                            </a>
                        </div>
                    </div>
                    <div class="col-2 col-sm-4 align-right" >
                        <div class="btn btn-search" id="btn-search-responsive">
                            <i class="fa fa-search"></i>
                        </div>
                        <basket-products></basket-products>
                    </div>
                </div>
            </div>
        </header>  
        <div id="website-content">
            <div id="menu_overlay"></div>
            <header id="top-header">
                <div class="container">
                    <div class="row vertical-align">
                        <div class="col-md-4 col-sm-12">
                            
                            <ul class="list-actions"> 
                                <li id="screen_menu_header_collapse">
                                    <a class="business-tag" target="_blank">
                                        <i class="fa fa-align-justify"></i> 
                                    </a>
                                </li>
                                <li class="call">
                                    <a class="business-tag" href="tel://{{$options['BUSINESS_PHONE_NUMBER']}}" target="_blank">
                                        <i class="fa fa-phone"></i>
                                        <span>{{$options['BUSINESS_PHONE_NUMBER']}}</span>
                                    </a>
                                </li>
                                <li class="whatsapp">
                                    <a class="business-tag" href="{{$options['BUSINESS_WHATSAPP_URL']}}"> 
                                        <i class="fab fa-whatsapp"></i> 
                                    </a>
                                </li>
                                <li id="sign_in_register"> 
                                    <sign-in-register 
                                    v-bind:prevent_text_collapse='false' csrf_token='{{csrf_token()}}'></sign-in-register>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-12 cc-center">
                            <a href="/">
                                <img class="img-logo" src="{{ URL::to(env('BUSINESS_IMG_LOGO')) }}" />
                            </a>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="list-top-right-actions">
                                <div id="input-search-container">
                                    <div id="input-search"> 
                                        <input class="form-control home-search" type="text" name="global_search" placeholder="@lang('website.what_are_you_looking_for')"> 
                                        <div id="recognition-search" class="input-group-append">
                                            <recognition-search></recognition-search>
                                        </div>
                                        <div class="btn btn-close" id="btn-search-responsive-close">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="basket_products">
                                    <basket-products></basket-products>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"> 
                        </div>
                        <div class="col-md-4"> 
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                </div>
            </header>
            <main>