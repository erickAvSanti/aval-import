@include('layouts.top_header')
<div class="container">
    @include('layouts.content_header')  
        <div class="card category-state">
            <div class="card-body2">
		    	@if(isset($cat_parent)) 
                    <i class="fa fa-home"></i>
                    <i class="fa fa-chevron-right"></i>
		    		<a href="/category-{{$cat_parent->id}}-{{str_slug($cat_parent->name)}}">{{ucfirst($cat_parent->name)}}</a>&nbsp;>&nbsp;
		    		<a href="/category-{{$cat_id}}-{{str_slug($cat_name)}}">{{ucfirst($cat_name)}}</a> 
		    	@else
                    <i class="fa fa-home"></i>
                    <i class="fa fa-chevron-right"></i>
		    		<a href="/category-{{$cat_id}}-{{str_slug($cat_name)}}">{{ucfirst($cat_name)}}</a> 
		    	@endif
            </div>
        </div> 
    @if(count($products)>0)
        @include('layouts.list_products_by_category')
    @else
    	<p>@lang('website.unregistered_products')</p>
    @endif
</div>
@include('layouts.footer')