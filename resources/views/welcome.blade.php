@include('layouts.top_header')
<div class="container">
    @include('layouts.content_header') 
</div>
<div id="header-owl-carousel" class="owl-carousel owl-theme">
    @forelse ($covers as $cover)
        @if($cover->visible==1)
            @if (isset($cover->url) and strlen($cover->url)>0)
                <a href="{{$cover->url}}">
                    <div class="owl-slide" style="background-image: url('{{URL::to('storage/covers/'.$cover->image_name)}}')"> 
                    </div>  
                </a>
            @else
                <div class="owl-slide" style="background-image: url('{{URL::to('storage/covers/'.$cover->image_name)}}')"> 
                </div> 
            @endif
        @endif
    @empty
        <div class="owl-slide" style="background-image: url('{{URL::to('img/portada.jpg')}}')">
            <div class="owl--text">bla bla bla</div> 
        </div>  
    @endforelse
</div> 
<div class="container">
    @include('layouts.list_products_by_category')
    <div style="text-align: center;">
        <b>Fotos referenciales. </b>Todos nuestros precios incluyen impuestos.<br />
        Toda entrega se aplicará costo de delivery el cual dependerá de la zona.
    </div> 
    
</div>
@include('layouts.footer')
<!--
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif 
</div>
-->
