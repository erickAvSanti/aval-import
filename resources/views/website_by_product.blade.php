@include('layouts.top_header')
<div class="container">
    @include('layouts.content_header') 
    <div class="container">
        @if(count($prod_categories)==1)
        <div class="card category-state">
            <div class="card-body2">
                @foreach ($prod_categories as $prod_cat)
                    @if(isset($prod_cat->parent))
                        <a href="/category-{{$prod_cat->parent->id}}-{{str_slug($prod_cat->parent->name)}}">{{$prod_cat->parent->name}}</a>&nbsp;>&nbsp;
                        <a href="/category-{{$prod_cat->id}}-{{str_slug($prod_cat->name)}}">{{$prod_cat->name}}</a>  
                    @else
                        <a href="/category-{{$prod_cat->id}}-{{str_slug($prod_cat->name)}}">{{$prod_cat->name}}</a> 
                    @endif
                @endforeach
            </div>
        </div>
        @else
        @endif  
        <div class="row">
            @if(count($images)>0)
                <div class="col-sm-6">
                    <div id="owl-carousel-product" class="owl-carousel owl-theme"> 
                        @foreach ($images as $img)
                            <img class="owl-lazy" data-src="/{{$folder_image_products.$img->file_name}}" alt="{{$img->file_name}}"> 
                        @endforeach 
                    </div>
                </div>
                <div class="col-sm-6">
                    @include('layouts.product_details')
                </div>
            @else
                <div class="col-md-12">
                    @include('layouts.product_details')
            @endif
        </div> 
    </div>   
</div>
@include('layouts.footer')