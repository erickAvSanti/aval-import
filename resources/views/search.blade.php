@include('layouts.top_header')
<div class="container">
    @include('layouts.content_header')
    <h5 class="title20">@lang('website.search_results'):&nbsp;<b>"</b>{{$text}}<b>"</b></h5>  
	@include('layouts.list_products')
</div>
@include('layouts.footer')