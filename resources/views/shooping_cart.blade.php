@include('layouts.top_header')
<div class="container">
    @include('layouts.content_header')  
		@if(count($products)>0)
	        <div id="selected_products" class="row">
	        	<div class="col-lg-7 col-md-8 col-sm-12">
	        		<div>
	        			<shopping-cart-form></shopping-cart-form>
	        		</div>
	        	</div>
	        	<div class="col-lg-5 col-md-4 col-sm-12 items">
		        	<ul>
		            @foreach ($products as $product) 
							@if($product->first_image) 
								@php
									$image_src = URL::to($folder_image_products.$product->first_image->file_name);
								@endphp
							@else 
								@php
									$image_src = '';
								@endphp
					    	@endif 
					    	<shopping-cart-item 
					    		:product_sku="0"
					    		:product_id="{{$product->id}}"
					    		url="/product/{{$product->id."-".str_slug($product->name)}}"
					    		image_src="{{$image_src}}"  
					    		product_name = "{{$product->name}}" 
					    		v-bind:price_unit_local = "{{$product->price_local}}" 
					    		currency_local = "{{env('CURRENCY_LOCAL')}}" 
					    		price_unit_local_text = "@lang('website.unit_price') {{env('CURRENCY_LOCAL')}} {{$product->price_local}}"
					    		v-bind:price_unit_foreign = "{{$product->price_foreign}}" 
					    		currency_foreign = "{{env('CURRENCY_FOREIGN')}}" 
					    		price_unit_foreign_text = "{{env('CURRENCY_FOREIGN')}} {{$product->price_foreign}}"
					    	>
					    		
					    	</shopping-cart-item> 

					@endforeach
		        	</ul>
		        </div>
		   	</div>
    	@else
    		<div id="shopping_cart_empty">
    			<div>
    				<strong><i class="fa fa-shopping-cart"></i></strong>
    				<span>@lang('website.shopping_cart_empty')</span>
    			</div>
    		</div>
    	@endif 
</div>
@include('layouts.footer')