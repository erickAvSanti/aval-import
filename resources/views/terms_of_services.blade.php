@include('layouts.top_header')
<div class="container">
    @include('layouts.content_header')  
    <br /> 
    <h4>@lang('website.terms_of_services')</h4>
    <div>{!!$text!!}</div>
</div>
@include('layouts.footer')