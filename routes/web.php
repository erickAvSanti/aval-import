<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', 'WebsiteController@index');
/*
Route::get('/category-all-{id}-{name}', 'WebsiteController@show_by_category')
	->where(['id'=>'[\d]+','name'=>'[\w-]+']);
*/
Route::get('/category-{id}-{name}', 'WebsiteController@show_by_category')
	->where(['id'=>'[\d]+','name'=>'[\w-]+']);
Route::get('/product/{id}-{name}', 'WebsiteController@show_product')
	->where(['id'=>'[\d]+','name'=>'[\w-]+']);

/*
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
*/
Route::get('/listCategories', 'CategoryController@list');
Route::get('/listWithParentCategories', 'CategoryController@list_with_parents');
Route::get('/listParentWithChildrenCategories', 'CategoryController@list_parent_with_children');
Route::get('/shopping-cart', 'ShoopingCartController@show');
Route::get('/terms-of-services', 'WebsiteController@terms_of_services');
Route::get('/search', 'SearchController@search');

//Route::resource('covers', 'CoverController');