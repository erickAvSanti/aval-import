<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/



Route::group(['middleware' => ['cors']], function () {

	Route::prefix('adm/users')->group(function () { 

		Route::post('login', 'PassportController@login');
		//Route::post('register', 'PassportController@register'); 
		//Route::put('/{id}', 'PassportController@update');
		Route::group(['middleware' => ['auth:api']], function () {
			$regex_id_int = '^[\d]+$';
		    Route::get('profile', 'PassportController@details'); 
		    Route::get('/', 'PassportController@list'); 
		    Route::put('/{id}', 'PassportController@update')->where(['id'=>$regex_id_int]);
		    Route::post('/', 'PassportController@register'); 
		    Route::delete('/{id}', 'PassportController@destroy')->where(['id'=>$regex_id_int]); 
		    Route::post('/logout', 'PassportController@logout'); 
		}); 
	
	});

	Route::prefix('clients')->group(function () { 

		Route::post('register', 'ClientsController@register');
	
	});

	Route::prefix('adm')->group(function () {  
		Route::group(['middleware' => ['auth:api']], function () { 
			$regex_id_int = '^[\d]+$';
			Route::get('covers', 'CoverController@list');
			Route::get('covers_images', 'CoverController@images');
			Route::delete('covers_image/{src}', 'CoverController@remove_image');
			Route::post('covers', 'CoverController@store');
			Route::put('covers/{id}', 'CoverController@update')->where(['id'=>$regex_id_int]);
			Route::post('covers/images_upload', 'CoverController@images_upload');
			Route::delete('covers/{id}', 'CoverController@destroy')->where(['id'=>$regex_id_int]);


			Route::get('categories', 'CategoryController@list');
			Route::get('categories_children/{id}', 'CategoryController@children')->where(['id'=>$regex_id_int]);
			Route::post('categories', 'CategoryController@store');
			Route::put('categories/{id}', 'CategoryController@update')->where(['id'=>$regex_id_int]); 
			Route::delete('categories/{id}', 'CategoryController@destroy')->where(['id'=>$regex_id_int]);

			Route::post('categories_sub', 'CategoryController@store_sub');
			Route::put('categories_sub/{id}', 'CategoryController@update_sub')->where(['id'=>$regex_id_int]); 
			Route::delete('categories_sub/{id}', 'CategoryController@destroy_sub')->where(['id'=>$regex_id_int]);


			Route::get('products', 'ProductsController@list');
			Route::put('products_highlight/{id}', 'ProductsController@highlight')->where(['id'=>$regex_id_int]);
			Route::get('products_images', 'ProductsController@images');
			Route::delete('products_image/{src}', 'ProductsController@remove_image');
			Route::post('products', 'ProductsController@store');
			Route::put('products/{id}', 'ProductsController@update')->where(['id'=>$regex_id_int]);
			Route::post('products/images_upload', 'ProductsController@images_upload');
			Route::delete('products/{id}', 'ProductsController@destroy')->where(['id'=>$regex_id_int]);

			Route::get('options', 'OptionController@list');
			Route::post('options', 'OptionController@store');
			Route::put('options/{id}', 'OptionController@update')->where(['id'=>$regex_id_int]);
			Route::delete('options/{id}', 'OptionController@destroy')->where(['id'=>$regex_id_int]);
			Route::post('text-terms-of-services', 'TextController@saveTermsOfServices');
			Route::get('text-terms-of-services', 'TextController@getTermsOfServices');

		}); 
	
	});
}); 

/*
Route::group(['middleware' => ['cors']], function () {

	Route::prefix('adm')->group(function () { 

		$regex_id_int = '^[\d]+$';

		Route::get('covers', 'CoverController@list');
		Route::get('covers_images', 'CoverController@images');
		Route::delete('covers_image/{src}', 'CoverController@remove_image');
		Route::post('covers', 'CoverController@store');
		Route::put('covers/{id}', 'CoverController@update')->where(['id'=>$regex_id_int]);
		Route::post('covers/images_upload', 'CoverController@images_upload');
		Route::delete('covers/{id}', 'CoverController@destroy')->where(['id'=>$regex_id_int]);


		Route::get('categories', 'CategoryController@list');
		Route::get('categories_children/{id}', 'CategoryController@children')->where(['id'=>$regex_id_int]);
		Route::post('categories', 'CategoryController@store');
		Route::put('categories/{id}', 'CategoryController@update')->where(['id'=>$regex_id_int]); 
		Route::delete('categories/{id}', 'CategoryController@destroy')->where(['id'=>$regex_id_int]);

		Route::post('categories_sub', 'CategoryController@store_sub');
		Route::put('categories_sub/{id}', 'CategoryController@update_sub')->where(['id'=>$regex_id_int]); 
		Route::delete('categories_sub/{id}', 'CategoryController@destroy_sub')->where(['id'=>$regex_id_int]);


		Route::get('products', 'ProductsController@list');
		Route::put('products_highlight/{id}', 'ProductsController@highlight')->where(['id'=>$regex_id_int]);
		Route::get('products_images', 'ProductsController@images');
		Route::delete('products_image/{src}', 'ProductsController@remove_image');
		Route::post('products', 'ProductsController@store');
		Route::put('products/{id}', 'ProductsController@update')->where(['id'=>$regex_id_int]);
		Route::post('products/images_upload', 'ProductsController@images_upload');
		Route::delete('products/{id}', 'ProductsController@destroy')->where(['id'=>$regex_id_int]);

		Route::get('options', 'OptionController@list');
		Route::post('options', 'OptionController@store');
		Route::put('options/{id}', 'OptionController@update')->where(['id'=>$regex_id_int]);
		Route::delete('options/{id}', 'OptionController@destroy')->where(['id'=>$regex_id_int]);
		Route::post('text-terms-of-services', 'TextController@saveTermsOfServices');
		Route::get('text-terms-of-services', 'TextController@getTermsOfServices');

	});
}); 
*/